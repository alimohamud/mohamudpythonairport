#Main class
#
#This class runs input_mod.py


import sys
import input_mod 

if __name__ == "__main__":
    
    
    if len(sys.argv) < 2 :
        print("Usage: python3 main.py filename")
        sys.exit(1)
    filename = sys.argv[1]
    input_mod.file_splitter(filename)
    input_mod.priority_sort()
