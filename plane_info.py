#Plane Info class
# Change name
#This class is a data structure that deals with the plane's submissions and inputs.

class PlaneInfo:

    def __init__(self, plane_id, submission_time, request_start, request_duration):
        self.id = str(plane_id)
        self.submission_time= int(submission_time)
        self.request_start= int(request_start)
        self.request_duration = int(request_duration)
        self.start = 0
        self.end = 0

    """
    Gets the plane ID
    """
    def get_id(self):
        return self.id
    
    """
    Gets the submission time
    """
    def get_submission_time(self):
        return self.submission_time

    """
    Gets the requested start time
    """
    def get_request_start(self):
        return self.request_start

    """
    Gets the requested duration
    """
    def get_requested_duration(self):
        return self.request_duration
    
    def __str__(self):
        return "[" + self.id + ", " + str(self.submission_time) + ", " + str(self.request_start) + \
               ", " + str(self.request_duration) + "]"
    
    def __repr__(self):
        return str(self)
    
    """
    Gets the actual takeoff time
    """
    def get_start(self):
        return self.start
    
    """
    Gets the end of takeoff time
    """
    def get_end(self):
        return self.end
    """
    Sets the actual takeoff time
    """
    def set_start(self, start):
        self.start =start

    """
    Sets the end of takeoff time
    """
    def set_end(self, end):
        self.end = end


    
def test_plane():    
    plane = PlaneInfo('Delta 5', '0', '4', '6')
    print(plane)


    
if __name__ == "__main__":
    
    test_plane()
    
    


    
