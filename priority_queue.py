#Priority Queue class
#
#This class uses a priority queue to sort the planes using Python's heapq.
#The queue has two priorities first submission time then request start time.

import heapq

class PriorityQueue:
    def __init__(self):
        self.__queue = []
        self.__index = 0
        
    

    def pop(self):
        return heapq.heappop(self.__queue)
        self.__index -= 1


    def push(self, obj, sub_time, req_start):
        return heapq.heappush(self.__queue, (sub_time, req_start,obj))
        self.__index += 1
                              
    

        
