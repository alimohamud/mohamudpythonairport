
#Testing file
#
#This tests various functions within the program

import sys
import simulator
import plane_info
"""
This file tests each of the functions in this program. to test any specific
function just uncomment test_functionname() in the main below.
"""



"""
Tests the plane_info object
"""
def test_plane():    
    plane = plane_info.PlaneInfo('Delta 5', '0', '4', '6')
    print(plane)
"""
Tests file_splitter in inout_mod
"""
def test_file_splitter():
    fs = simulator.file_splitter(filename)
    print(fs)
"""
Tests the info_formatter method in simulator.py
"""
def test_info_formatter():
    test_info = simulator.info_formatter()
    print(test_info)

"""
Tests the requst_sort method in simulator.py
"""
def test_request_sort():
    rs = simulator.request_sort()
    print(rs)
"""
Tests set_takeoffs in simulator.py
"""
def test_set_takeoffs():
    ts = simulator.set_takeoffs()
    for i in range(len(ts)):
        print(ts[i].get_start())
        print(ts[i].get_end())

"""
Tests timeslot_printer in simulator.py
"""
def test_timeslot_printer():
    tsp = simulator.timeslot_printer()
    print(tsp)

"""
Tests timeslot_summary in simulator.py
"""
def test_timeslot_summary():
    tss = simulator.timeslot_summary()
    print(tss)
    
if __name__ == "__main__":
    
    #test_plane()
    
    if len(sys.argv) < 2 :
        print("Usage: python3 main.py filename")
        sys.exit(1)
    filename = sys.argv[1]
    simulator.file_splitter(filename)
    #test_file_splitter()
    #test_info_formatter()
    #test_request_sort()
    #test_set_takeoffs()
    test_timeslot_printer()
    test_timeslot_summary()
      
##    simulator.info_formatter()
##    simulator.priority_sort()
##    simulator.set_takeoffs()
   
##    
##    simulator.timeslot_printer(0)
##    simulator.timeslot_printer(4)
##    simulator.timeslot_summary()
