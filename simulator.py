#Simulator
#
#This class will read in the input file then seperate and format the plane data

import plane_info
import sys               

"""
file_splitter checks if the file is valid by looking at what type of file is
being passed in. This program only works with .csv and .txt files so the
filenames must end with either .csv or .txt. If that works it takes in the
file splitting each line into its own list"

Parameters
----------
filename
    name of the .csv or .txt file going into the program

Return
------
plane_list
    A list of filename's data.
"""

def file_splitter(filename):
    if filename.endswith('.csv') or filename.endswith('.txt'):
        a_file = open(filename, "r")
        plane_list = []
        for line in a_file:
            plane_line = line.strip()
            temp_list = plane_line.split(',')
            tl1 = int(temp_list[1])
            tl2 = int(temp_list[2])
            tl3 = int(temp_list[3])
            
            plane_list.append(temp_list)
            #print(temp_list)

        a_file.close()
        return plane_list
        
    else:
        print("Only .txt and .csv files allowed")


"""
Creates plane info object by passing in the plane_list filled out by the
file_splitter method above. Traverses each list in plane_list after confirming
it has enough information to create the plane object.

No parameters but runs file_splitter.

Return
------
info_list
    A list of plane objects
"""
def info_formatter():
    plane_list = file_splitter(sys.argv[1])
    info_list = []
    for obj in plane_list:
            if len(obj) == 4:
                for info in obj:
                    plane = plane_info.PlaneInfo(obj[0], obj[1], obj[2], obj[3])
                info_list.append(plane)
                #print(plane)
    return info_list




"""
This function sorts the list returned from info_formatter() first by the requested start time,
if the requested starts are equal it then places the plane with the earlier
submission time ahead on the list.

No parameters but runs info_formatter()

Return
------
sorted_list
    A list first sorted by requested start time then submission time

"""
def request_sort():
    sorted_list = info_formatter()
    sorted_list.sort(key=lambda req : (req.request_start, req.submission_time))
    
    
    return sorted_list

"""
This function takes the sorted list from request_sort() and sets the takeoff times
for each plane accordingly.

No parameters but runs request_sort()

Return
------
wait_list:
    A list of the sorted planes but now with start and end times
"""
def set_takeoffs():
    wait_list = request_sort()
    if len(wait_list) > 0:
        wait_list[0].set_start(wait_list[0].get_request_start())
        for i in range(1, len(wait_list)):
            diff = wait_list[i - 1].get_start() + wait_list[i - 1].get_requested_duration()
            if diff > wait_list[i].get_request_start():
                wait_list[i].set_start(diff)
            else:
               wait_list[i].set_start(wait_list[i].get_start())
        for i in range(len(wait_list)):
            wait_list[i].set_end(wait_list[i].get_start()-1 + wait_list[i].get_requested_duration())
    
    return wait_list
        
        
        
"""
This function prints off the lsit at every time step.
No parameters but it runs set_takeoffs() and uses the list generated from that function.
No returns but it prints the list.
"""
def timeslot_printer():
    q_list = set_takeoffs()
    ts_str = ""
    total_time = q_list[-1].get_end()
    i = 0
    r = 0
    if len(q_list) > 0:
        #if cq_list[0].get_end() <= timeslot:
        
        while i <= total_time:          #i is the current time
            
            ts_str = "At time " + str(i) + " the queue would look like: " 
            
            for j in range(r, len(q_list)):
                
                if i > q_list[j].get_end():
                    
                    r += 1
                
                if q_list[j].get_start() <= total_time:
                    ts_str += q_list[j].get_id() + " (started at " + str(q_list[j].get_start()) + "), "
                else:
                    ts_str += q_list[j].get_id() + " (scheduled for " + str(q_list[j].get_start()) + "), "
            
            print(ts_str)    
            i += 1
    else:
        ts_str += "Currently empty"
        print(ts_str)
           

def timeslot_summary():
    f_list = set_takeoffs()
    summ = ""
    if len(f_list) > 0:
        for i in range(len(f_list)):
            summ += f_list[i].get_id() + " (" + str(f_list[i].get_start()) + "-" + str(f_list[i].get_end()) + "), "
    else:
        summ += "No planes took off"
    return summ

def simulate():
    wait_list = set_takeoffs()
    taxi_list = []
    
    print(timeslot_summary())

